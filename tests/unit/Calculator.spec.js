import { mount } from '@vue/test-utils';

import Calculator from '../../src/components/Calculator.vue';

describe('Unit tests for Calculator component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(Calculator);
  });

  test('Changing input should motify data model', () => {
    const textInput = wrapper.find('input');
    textInput.setValue(0.9);

    expect(wrapper.vm.cash).toBe('0.9');
  });

  test('Changing data model should calculate computed property', () => {
    wrapper.setData({ cash: '0.9' });

    expect(wrapper.vm.calculateLaastAmount).toBe(1);
  });

  test('Changing input should affect the output', () => {
    const textInput = wrapper.find('input');
    textInput.setValue(0.9);

    expect(wrapper.find('h2').text()).toBe('Sa saad osta 1 pakki laastu.');
  });

  test('Having too much cash results in proper message', () => {
    wrapper.setData({ cash: '99999' });

    expect(wrapper.find('h2').text()).toBe('Kui sul nii palju pappi on, siis mida sa siit otsid? Mine osta parem laastu!');
  });

  test('Giving exponential number results in proper message', () => {
    wrapper.setData({ cash: '1e5' });

    expect(wrapper.find('h2').text()).toBe('These are not the laast you\'re looking for');
  });

  test('Message not shown when cash input is empty', () => {
    wrapper.setData({ cash: '' });

    expect(wrapper.find('h2').exists()).toBe(false);
  });

  test('Having no cash should scold the user', () => {
    wrapper.setData({ cash: '0' });

    expect(wrapper.find('h2').text()).toBe('Kes ei tööta, see laastu ei pane');
  });

  test('Input spawns multiple images', () => {
    wrapper.setData({ cash: '2.6' });

    expect(wrapper.findAll('img').length).toBe(3);
  });

  test('Too many cash doesn\'t add more than 100 laast', () => {
    wrapper.setData({ cash: '999' });

    expect(wrapper.findAll('img').length).toBe(100);
  });

  test('Sneaky no income user trying to fool system', () => {
    wrapper.setData({ cash: '00.000' });

    expect(wrapper.find('h2').text()).toBe('Kes ei tööta, see laastu ei pane');
  });
});
